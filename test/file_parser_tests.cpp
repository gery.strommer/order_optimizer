#include "order_optimizer/file_parser.h"
#include "order_optimizer/type_definitions.h"
#include <gtest/gtest.h>

std::string correct_path;

TEST(DirCheck, empty_dir)
{
  FileParser fp;

  std::string path = "";
  ASSERT_EQ(false, fp.checkDirectory(path));

  SUCCEED();
}

TEST(DirCheck, non_existing_dir)
{
  FileParser fp;

  std::string path = "testestestest";
  ASSERT_EQ(false, fp.checkDirectory(path));
  
  SUCCEED();
}

TEST(DirCheck, wrong_dir)
{
  FileParser fp;

  std::string path = "/home";
  ASSERT_EQ(false, fp.checkDirectory(path));
  
  SUCCEED();
}

TEST(DirCheck, correct_dir)
{
  FileParser fp;

  ASSERT_EQ(true, fp.checkDirectory(TEST_RESOURCE_DIR));
  
  SUCCEED();
}

TEST(ProductCheck, parse_products)
{
  FileParser fp;

  std::map<unsigned int, Product> known_products;

  ASSERT_EQ(true, fp.checkDirectory(TEST_RESOURCE_DIR));
  
  fp.parseConfiguration(&known_products);

  ASSERT_EQ("Product 1", known_products[1].name_);
  ASSERT_EQ("Part A", known_products[1].parts_.front().name_);
  ASSERT_EQ("Product 2", known_products[2].name_);
  ASSERT_EQ("Part B", known_products[2].parts_[1].name_);

  SUCCEED();
}

TEST(OrderCheck, parse_orders)
{
  FileParser fp;
  Order order;
  order.order_id_ = 1000001;

  ASSERT_EQ(true, fp.checkDirectory(TEST_RESOURCE_DIR));
  
  ASSERT_EQ(true, fp.parseOrders(&order));
  ASSERT_EQ((unsigned int)902, order.products_[0]);
  SUCCEED();
}

int main(int argc, char** argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  auto res = RUN_ALL_TESTS();
  return res;
}