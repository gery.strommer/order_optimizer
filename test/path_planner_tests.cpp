#include "order_optimizer/path_planner.h"
#include "order_optimizer/type_definitions.h"
#include <gtest/gtest.h>

TEST(PathPlannerCheck, plan_path_1)
{
  GreedyPathPlanner gpp;

  Point start, goal;
  Part part;
  std::map <std::string, Part> parts;
  std::vector<Part> path;

  // setup test
  start.x_ = 0;
  start.y_ = 0;
  goal.x_ = 100;
  goal.y_ = 100;

  part.name_ = "Test Part A";
  part.product_ids_.insert(1);
  part.product_ids_.insert(2);
  part.quantity_ = 10;
  part.dest_x_ = 50;
  part.dest_y_ = 50;
  parts.insert(std::make_pair(part.name_, part));

  part.name_ = "Test Part B";
  part.product_ids_.insert(3);
  part.product_ids_.insert(4);
  part.quantity_ = 5;
  part.dest_x_ = 25;
  part.dest_y_ = 25;
  parts.insert(std::make_pair(part.name_, part));

  gpp.plan(start, goal, parts, &path);

  EXPECT_FLOAT_EQ(0, path[0].dest_x_);
  EXPECT_FLOAT_EQ(0, path[0].dest_y_);

  EXPECT_FLOAT_EQ(25, path[1].dest_x_);
  EXPECT_FLOAT_EQ(25, path[1].dest_y_);
  ASSERT_EQ("Test Part B", path[1].name_);
  ASSERT_EQ((unsigned int)5, path[1].quantity_);

  EXPECT_FLOAT_EQ(50, path[2].dest_x_);
  EXPECT_FLOAT_EQ(50, path[2].dest_y_);
  ASSERT_EQ("Test Part A", path[2].name_);
  ASSERT_EQ((unsigned int)10, path[2].quantity_);
  
  EXPECT_FLOAT_EQ(100, path[3].dest_x_);
  EXPECT_FLOAT_EQ(100, path[3].dest_y_);

  SUCCEED();
}

TEST(PathPlannerCheck, plan_path_2)
{
  GreedyPathPlanner gpp;

  Point start, goal;
  Part part;
  std::map <std::string, Part> parts;
  std::vector<Part> path;

  // setup test
  start.x_ = 0;
  start.y_ = 0;
  goal.x_ = 100;
  goal.y_ = 100;

  part.name_ = "Test Part A";
  part.product_ids_.insert(1);
  part.product_ids_.insert(2);
  part.quantity_ = 10;
  part.dest_x_ = -50;
  part.dest_y_ = -50;
  parts.insert(std::make_pair(part.name_, part));

  part.name_ = "Test Part B";
  part.product_ids_.insert(3);
  part.product_ids_.insert(4);
  part.quantity_ = 5;
  part.dest_x_ = 150;
  part.dest_y_ = 150;
  parts.insert(std::make_pair(part.name_, part));

  gpp.plan(start, goal, parts, &path);

  EXPECT_FLOAT_EQ(0, path[0].dest_x_);
  EXPECT_FLOAT_EQ(0, path[0].dest_y_);

  EXPECT_FLOAT_EQ(-50, path[1].dest_x_);
  EXPECT_FLOAT_EQ(-50, path[1].dest_y_);
  ASSERT_EQ("Test Part A", path[1].name_);
  ASSERT_EQ((unsigned int)10, path[1].quantity_);

  EXPECT_FLOAT_EQ(150, path[2].dest_x_);
  EXPECT_FLOAT_EQ(150, path[2].dest_y_);
  ASSERT_EQ("Test Part B", path[2].name_);
  ASSERT_EQ((unsigned int)5, path[2].quantity_);
  
  EXPECT_FLOAT_EQ(100, path[3].dest_x_);
  EXPECT_FLOAT_EQ(100, path[3].dest_y_);

  SUCCEED();
}

TEST(PathPlannerCheck, plan_path_3)
{
  GreedyPathPlanner gpp;

  Point start, goal;
  Part part;
  std::map <std::string, Part> parts;
  std::vector<Part> path;

  // setup test
  start.x_ = 100;
  start.y_ = 100;
  goal.x_ = 100;
  goal.y_ = 100;

  part.name_ = "Test Part A";
  part.product_ids_.insert(1);
  part.product_ids_.insert(2);
  part.quantity_ = 10;
  part.dest_x_ = -50;
  part.dest_y_ = -50;
  parts.insert(std::make_pair(part.name_, part));

  part.name_ = "Test Part B";
  part.product_ids_.insert(3);
  part.product_ids_.insert(4);
  part.quantity_ = 5;
  part.dest_x_ = 100;
  part.dest_y_ = 100;
  parts.insert(std::make_pair(part.name_, part));

  gpp.plan(start, goal, parts, &path);

  EXPECT_FLOAT_EQ(100, path[0].dest_x_);
  EXPECT_FLOAT_EQ(100, path[0].dest_y_);

  EXPECT_FLOAT_EQ(100, path[1].dest_x_);
  EXPECT_FLOAT_EQ(100, path[1].dest_y_);
  ASSERT_EQ("Test Part B", path[1].name_);
  ASSERT_EQ((unsigned int)5, path[1].quantity_);
  
  EXPECT_FLOAT_EQ(-50, path[2].dest_x_);
  EXPECT_FLOAT_EQ(-50, path[2].dest_y_);
  ASSERT_EQ("Test Part A", path[2].name_);
  ASSERT_EQ((unsigned int)10, path[2].quantity_);

  EXPECT_FLOAT_EQ(100, path[3].dest_x_);
  EXPECT_FLOAT_EQ(100, path[3].dest_y_);

  SUCCEED();
}

int main(int argc, char** argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  auto res = RUN_ALL_TESTS();
  return res;
}