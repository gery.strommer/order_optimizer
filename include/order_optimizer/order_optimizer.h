#ifndef ORDER_OPTIMIZER_H
#define ORDER_OPTIMIZER_H

#include <string>
#include <mutex>
#include <queue>
#include <thread>
#include <condition_variable>
#include <map>

#include "rclcpp/rclcpp.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "order_optimizer/msg/new_order.hpp" 
#include "visualization_msgs/msg/marker_array.hpp"

#include "order_optimizer/file_parser.h"
#include "order_optimizer/type_definitions.h"
#include "order_optimizer/path_planner.h"

using geometry_msgs::msg::PoseStamped;
using order_optimizer::msg::NewOrder;

/**
 * Class OrderOptimizer
 * ROS2 Node for order optimization
 * Subscribes to newOrder topic and plans a path for execution
 **/
class OrderOptimizer : public rclcpp::Node
{
  public:
    OrderOptimizer();
    ~OrderOptimizer();

    /**
     * callback for incomming robot position, overwrites current position in class object
     * @param msg: topic message
     **/
    void position_cb (geometry_msgs::msg::PoseStamped::SharedPtr msg);

    /**
     * callback for incomming orders, triggers order execution (runs in different thread)
     * @param msg: topic message
     **/
    void order_cb (order_optimizer::msg::NewOrder::SharedPtr msg);

  private:
    // subscribers and publishers
    rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr sub_position_;
    rclcpp::Subscription<order_optimizer::msg::NewOrder>::SharedPtr sub_order_;
    rclcpp::Publisher<visualization_msgs::msg::MarkerArray>::SharedPtr pub_vis_;

    // current robot pose and corresponding mutex
    PoseStamped current_pose_;
    std::mutex pose_lock_;
    bool got_position_;

    // thread and its function, variables for order processing
    std::thread t_order_process_;
    bool run_thread;
    std::queue<NewOrder> orders_;
    std::mutex orders_mtx_;
    std::condition_variable order_cv_;

    /**
     * Thread function for permanent order execution (if available)
     **/
    void processOrder();

    /**
     * helper function for file parsing
     * @param id: incoming order id, classifies order 
     * @param order_parts: data structure to write required parts
     * @param goal: extracted goal position
     **/
    bool getOrderParts(unsigned int id, std::map<std::string, Part>* order_parts, Point* goal);

    /**
     * helper function for path planning and visualization
     * @param goal: goal position
     * @param order_parts: required parts for order
     **/
    void planAndVisualize(Point goal, std::map<std::string, Part> order_parts);

    // object that takes care of file parsing
    FileParser file_parser_;
    
    // path planner
    GreedyPathPlanner path_planner_;

      // maps for saving data
    std::map<unsigned int, Product> known_products_;
};

#endif //ORDER_OPTIMIZER_H