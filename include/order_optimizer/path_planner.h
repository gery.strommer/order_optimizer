#ifndef PATH_PLANNER_ORDER_OPTIMIZER_H
#define PATH_PLANNER_ORDER_OPTIMIZER_H

#include "order_optimizer/type_definitions.h"
#include <string>
#include <map>
#include <vector>
#include <math.h>

/**
 * Class GreedyPathPlanner
 * Plans a path for part-pickup. 
 * Uses a greedy planning algorithm --> next position is always the nearest available
 **/
class GreedyPathPlanner
{
  public:
    GreedyPathPlanner();
    ~GreedyPathPlanner();

    /**
     * plan a path for part-pickup
     * @param start: start position
     * @param goal: goal position
     * @param parts: contains parts and their position that need to be picked up
     * @param path: containes planned path when finished
     **/
    void plan(Point start, Point goal, std::map <std::string, Part> parts, std::vector<Part>* path);

  private:
    float** link_distances_;
    int nr_nodes_;
    std::vector<std::pair<int, Part>> nodes_;

    /**
     * init planner, calcs distances between nodes (positions)
     * @param start: start position
     * @param goal: goal position
     * @param parts: contains parts and their position that need to be picked up
     **/
    void initPlanner(Point start, Point goal, std::map <std::string, Part> parts);

    /**
     * plan a path for part-pickup
     * @param path: containes planned path when finished
     **/
    void planPath(std::vector<Part>* path);
};

#endif //PATH_PLANNER_ORDER_OPTIMIZER_H