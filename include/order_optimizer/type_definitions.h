#ifndef ORDER_OPTIMIZER_ORDER_H
#define ORDER_OPTIMIZER_ORDER_H

#include <vector>
#include <string>
#include <unordered_set>

/**
 * Point
 * x and y coordinate of a point
 **/
typedef struct{
  float x_;
  float y_;
}Point;

/**
 * Order
 * contains information of an order
 **/
typedef struct
{
  unsigned int order_id_;
  float dest_x_;
  float dest_y_;
  std::vector<unsigned int> products_;
}Order;

/**
 * Part
 * needed for products within an order
 * quantity_ and product_ids_ are additional information
 **/
typedef struct
{
  std::string name_;
  float dest_x_;
  float dest_y_;
  unsigned int quantity_;
  std::unordered_set<unsigned int> product_ids_;
}Part;

/**
 * Product
 * needed for an order
 * contains parts needed for product
 **/
typedef struct 
{
  unsigned int id_;
  std::string name_;
  std::vector<Part> parts_;
}Product;




#endif //ORDER_OPTIMIZER_ORDER_H