#ifndef ORDER_OPTIMIZER_FILE_PARSER_H
#define ORDER_OPTIMIZER_FILE_PARSER_H

#include <string>
#include <filesystem>
#include <thread>
#include <vector>
#include <mutex>
#include <map>
#include <yaml-cpp/yaml.h>
#include "order_optimizer/msg/new_order.hpp" 
#include <atomic>

#include "order_optimizer/type_definitions.h"

using order_optimizer::msg::NewOrder;

#define ORDER_FOLDER_NAME "orders"
#define CFG_FOLDER_NAME "configuration"

/**
 * Class FileParser
 * Handles yaml-file parsing for OrderOptimizer
 **/
class FileParser
{
  public:
    FileParser();
    ~FileParser();

    /**
     * checks if directory is valid and contains necessary folders
     * @param path: directory path
     * @return: true if path & directory are valid, false otherwise
     **/
    bool checkDirectory(std::string path);

    /**
     * checks if order description is available in files
     * @param order: order that needs to be written with data from file, already contains order_id_ for parsing
     * @return: true if order could be found and order object has been updated
     **/
    bool parseOrders(Order* order);

     /**
     * load all known products and their parts
     * @param known_products: datastructure, where function adds products
     **/
    void parseConfiguration(std::map<unsigned int, Product>* known_products);

  private:
    std::string order_path_;
    std::string config_path_;
    
    // parsing threads and functions
    std::vector<std::thread> order_threads_;
    std::atomic<bool> order_found_;

    /**
     * function executed by generated thread, searches for wanted_order_ based on wanted_order_id_
     * @param path: absolute path of file to parse
     **/
    void parseOrderFile(std::string path);

    std::map<unsigned int, Order>* known_orders_;
    std::mutex order_mtx_;
    Order* wanted_order_;
    unsigned int wanted_order_id_;
};

#endif //ORDER_OPTIMIZER_FILE_PARSER_H