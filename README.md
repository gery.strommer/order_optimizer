# order_optimizer - Knapp Industry Solutions GmbH Candidate Evaluation

## Assumptions:
- Parts with same name are always on same location
- No coordinate frame transformations required (e.g. currentPosition topic)

## Remarks:
- Configuration file not as expected: 'product: "Product 1"' instead of 'product: "56"'

## Terminal Publish
- ros2 topic pub /nextOrder order_optimizer/msg/NewOrder "{order_id: 1000001, description: 'Test'}"
- ros2 topic pub /currentPosition geometry_msgs/msg/PoseStamped "header:
  stamp:
    sec: 0
    nanosec: 0
  frame_id: 'map'
pose:
  position:
    x: 0.0
    y: 0.0
    z: 0.0
  orientation:
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0 "

# Unit Tests
Run following command
- colcon test --packages-select order_optimizer --event-handlers=console_cohesion+

