import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    ld = LaunchDescription()
    config = os.path.join(
        get_package_share_directory('order_optimizer'),
        'config',
        'params.yaml'
        )
        
    node=Node(
        package = 'order_optimizer',
        name = 'OrderOptimizer',
        executable = 'OrderOptimizer',
        output="screen",
        parameters = [config],
        #prefix=['valgrind --leak-check=full --track-origins=yes'],
    )
    ld.add_action(node)

    node=Node(
        package = 'rviz2',
        name = 'rviz2',
        executable = 'rviz2',
        arguments=['-d' + os.path.join(get_package_share_directory('order_optimizer'), 'config', 'setup.rviz')]
    )
    ld.add_action(node)

    return ld
