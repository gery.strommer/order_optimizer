#include "order_optimizer/file_parser.h"
#include <iostream>

namespace fs = std::filesystem;

FileParser::FileParser() = default;

FileParser::~FileParser() = default;

bool FileParser::checkDirectory(std::string path)
{
  std::string compare_cfg = path + "/" + CFG_FOLDER_NAME, compare_order = path + "/" + ORDER_FOLDER_NAME;
  order_path_.clear();
  config_path_.clear();

  if(fs::exists(path))
  {
    // check if directory is valid
    for(auto& folder : fs::directory_iterator(path))
    {
      if(folder.path().compare(compare_order) == 0)
        order_path_ = folder.path();
      if(folder.path().compare(compare_cfg) == 0)
        config_path_ = folder.path();;
      
      if(!order_path_.empty() && !config_path_.empty())
        break;
    }
  }

  return !order_path_.empty() && !config_path_.empty();
}

bool FileParser::parseOrders(Order* order)
{
  std::string path;
  wanted_order_ = order;
  wanted_order_id_ = order->order_id_;
  order_found_ = false;

  // create a thread for each order file
  for(auto& file : fs::directory_iterator(order_path_))
  {
    path = file.path();
    std::thread parse_thread([this, path]
    {
        this->parseOrderFile(path);
    });
    order_threads_.push_back(std::move(parse_thread));
  }

  // wait for all orders to be parsed
  for(auto& thread : order_threads_)
  {
    if(thread.joinable())
      thread.join();
  }
  
  return (bool) order_found_;
}

void FileParser::parseConfiguration(std::map<unsigned int, Product>* known_products)
{
  Product product;
  Part part;

  for(auto& file : fs::directory_iterator(config_path_))
  {
    // load file with yaml-cpp
    YAML::Node yaml_file = YAML::LoadFile(file.path());

    // parse fields into product object
    for(std::size_t i = 0; i < yaml_file.size(); i++) 
    {    
      product.id_ = yaml_file[i]["id"].as<unsigned int>();
      product.name_ = yaml_file[i]["product"].as<std::string>();

      for(std::size_t p = 0; p < yaml_file[i]["parts"].size(); p++)
      {
        part.name_ = yaml_file[i]["parts"][p]["part"].as<std::string>();
        part.dest_x_ = yaml_file[i]["parts"][p]["cx"].as<float>();
        part.dest_y_ = yaml_file[i]["parts"][p]["cy"].as<float>();
        product.parts_.push_back(part);
      }

      // add product to known products
      known_products->insert(std::make_pair(product.id_, product));
      product.parts_.clear();
    }
  }
}

void FileParser::parseOrderFile(std::string path)
{
  Order order;
  
  // load file with yaml-cpp
  YAML::Node yaml_file = YAML::LoadFile(path);

  // parse fields into order object
  for (std::size_t i = 0; i < yaml_file.size(); i++) 
  { 
    // check if order has been found
    if(order_found_)
      break;
    else
    {
      order.order_id_ = yaml_file[i]["order"].as<unsigned int>();
      if(wanted_order_id_ == order.order_id_)
      { 
        order_found_ = true;
        order.dest_x_ = yaml_file[i]["cx"].as<float>();
        order.dest_y_ = yaml_file[i]["cy"].as<float>();
        for(std::size_t p = 0; p < yaml_file[i]["products"].size(); p++)
          order.products_.push_back(yaml_file[i]["products"][p].as<unsigned int>());

        // set order and break
        order_mtx_.lock();
        *wanted_order_ = order;
        order_mtx_.unlock();
        break;
      }
    }
  }
}