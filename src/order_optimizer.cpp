#include "order_optimizer/order_optimizer.h"
#include <iostream>

OrderOptimizer::OrderOptimizer() : Node("OrderOptimizer")
{
  sub_position_ = this->create_subscription<geometry_msgs::msg::PoseStamped>
                      ("currentPosition", 10, std::bind(&OrderOptimizer::position_cb, this, std::placeholders::_1));
  sub_order_ = this->create_subscription<order_optimizer::msg::NewOrder>
                  ("nextOrder", 10, std::bind(&OrderOptimizer::order_cb, this, std::placeholders::_1));
  pub_vis_ = this->create_publisher<visualization_msgs::msg::MarkerArray>("order_path", 10);

  // declare a ROS2 parameter than can be changed during runtime
  this->declare_parameter<std::string>("directory_path", std::string());

  // check if directory path and parse files
  std::string dir_path;
  this->get_parameter("directory_path", dir_path);
  if(dir_path.empty())
  {
    RCLCPP_ERROR(this->get_logger(), "Directory path is empty. Please set a path via ROS2 parameter 'directory_path'. Shutdown node....");
    rclcpp::shutdown();
  }
  else if(!file_parser_.checkDirectory(dir_path))
  {
    RCLCPP_ERROR(this->get_logger(), "Directory path is invalid. Please check the directory structure. Shutdown node....");
    rclcpp::shutdown();
  }  
  else
  {  
    // load configuration file once
    file_parser_.parseConfiguration(&known_products_);

    run_thread = true;
    t_order_process_ = std::thread(&OrderOptimizer::processOrder, this);
  }

  got_position_ = false;
}

OrderOptimizer::~OrderOptimizer()
{
  run_thread = false;

  // notify worker thread, may sleeps in wait for new order
  order_cv_.notify_one();
  // wait for thread to terminate
  t_order_process_.join();
}

void OrderOptimizer::position_cb(geometry_msgs::msg::PoseStamped::SharedPtr msg)
{
  pose_lock_.lock();
  got_position_ = true;
  current_pose_ = *msg;
  pose_lock_.unlock();
}

void OrderOptimizer::order_cb(order_optimizer::msg::NewOrder::SharedPtr msg)
{
  std::unique_lock<std::mutex> lock(orders_mtx_);

  bool notify = orders_.empty();
  orders_.push(*msg);

  lock.unlock();

  // worker thread only needs to be notified if queue was empty
  if(notify)
    order_cv_.notify_one();
}

void OrderOptimizer::processOrder()
{
  NewOrder new_order;
  Point goal;
  std::map<std::string, Part> order_parts;

  while(run_thread)
  {
    std::unique_lock<std::mutex> lock(orders_mtx_);
    if(orders_.empty())
      order_cv_.wait(lock);

    if(!run_thread)
      return;

    new_order = orders_.front();
    orders_.pop();
  
    lock.unlock();

    std::cout << "Working on order " << new_order.order_id << " - " << new_order.description << std::endl;

    // get required parts for order and plan if possible    
    if(getOrderParts(new_order.order_id, &order_parts, &goal))
      planAndVisualize(goal, order_parts);

    // clean up
    order_parts.clear();
  }
}

bool OrderOptimizer::getOrderParts(unsigned int id, std::map<std::string, Part>* order_parts, Point* goal)
{
  Order order;
  
  // parse all orders from directory
  order.order_id_ = id;

  // check if incomming order is valid
  if(file_parser_.parseOrders(&order))
  {
    // check for required products
    for(auto req_prod_id : order.products_)
    {
      // search for product in known products
      auto product = known_products_.find(req_prod_id);
      if(product != known_products_.end())
      { 
        // add parts needed for product
        for(auto part : product->second.parts_)
        {            
          auto pos = order_parts->find(part.name_);
          if (pos == order_parts->end()) 
          {
            part.quantity_ = 1;
            part.product_ids_.insert(product->second.id_);
            order_parts->insert(std::make_pair(part.name_, part));
          }
          else 
          {
            pos->second.quantity_++;
            pos->second.product_ids_.insert(product->second.id_);
          }
        }
      }
      else
      {
        RCLCPP_WARN(this->get_logger(), "Product with ID %d from received order with ID %d is not known. Please add this product to the corresponding file!", req_prod_id, id);
        return false;
      }
    }
    goal->x_ = order.dest_x_;
    goal->y_ = order.dest_y_;
    return true;
  }
  else
    RCLCPP_WARN(this->get_logger(), "Received order with ID %d is not known. Please add this order to the corresponding file!", id);

  return false;
}

void OrderOptimizer::planAndVisualize(Point goal, std::map<std::string, Part> order_parts)
{
  Point start;
  bool valid_pos;
  std::vector<Part> path;
  visualization_msgs::msg::MarkerArray path_markers;
  visualization_msgs::msg::Marker marker;

  // get current position
  pose_lock_.lock();
  start.x_ = current_pose_.pose.position.x;
  start.y_ = current_pose_.pose.position.y;
  marker.header.frame_id = current_pose_.header.frame_id;
  valid_pos = got_position_;
  pose_lock_.unlock();

  // if current position is invalid, abort planning
  if(!valid_pos)
  {
    RCLCPP_WARN(this->get_logger(), "No current position received. Please publish on corresponding topic!");
    return;
  }

  // perform path planning
  path_planner_.plan(start, goal, order_parts, &path);

  // print and visualize path
  path_markers.markers.resize(path.size());
  Part path_part;
  for(std::size_t i = 0; i < path.size(); i++)
  {
    path_part = path[i];

    marker.header.stamp = this->get_clock()->now();
    marker.action = visualization_msgs::msg::Marker::ADD;
    marker.id = i;
    marker.pose.position.x = path_part.dest_x_;
    marker.pose.position.y = path_part.dest_y_;
    marker.pose.position.z = 0;
    marker.scale.x = .5;
    marker.scale.y = .5;
    marker.scale.z = .5;
    marker.color.a = 1.0;

    if(i == 0)
    {
      marker.type = visualization_msgs::msg::Marker::CUBE;
      marker.color.r = 0.0;
      marker.color.g = 1.0;
      marker.color.b = 0.0;
      std::cout << "Start at current position x: " << path_part.dest_x_ << ", y: " << path_part.dest_y_ << std::endl;
    }
    else if(i == path.size()-1)
    {
      marker.type = visualization_msgs::msg::Marker::CUBE;
      marker.color.r = 1.0;
      marker.color.g = 0.0;
      marker.color.b = 0.0;
      std::cout << "Delivering to destination x:" << path_part.dest_x_ << ", y: " << path_part.dest_y_ << std::endl << std::endl;
    }
    else
    {
      marker.type = visualization_msgs::msg::Marker::CYLINDER;
      marker.color.r = 0.0;
      marker.color.g = 0.0;
      marker.color.b = 1.0;
      std::cout << "Fetching " << path_part.quantity_ << " parts of name " << path_part.name_ << " for products ";
      for(auto id : path_part.product_ids_) 
        std::cout << id << " ";
      std::cout << " at x: " << path_part.dest_x_ << ", y: " << path_part.dest_y_ << std::endl;
    }

    path_markers.markers[i] = marker;      
  }

  // publish path
  pub_vis_->publish(path_markers);
}