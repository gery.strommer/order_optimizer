#include "order_optimizer/path_planner.h"

GreedyPathPlanner::GreedyPathPlanner()
{
  link_distances_ = nullptr;
}

GreedyPathPlanner::~GreedyPathPlanner()
{
  if(link_distances_)
  {
    for (int i = 0; i < nr_nodes_; i++)
      delete[] link_distances_[i];
    delete[] link_distances_;
  }
  link_distances_ = nullptr;
}

void GreedyPathPlanner::plan(Point start, Point goal, std::map <std::string, Part> parts, std::vector<Part>* path)
{
  // init and plan
  initPlanner(start, goal, parts);
  planPath(path);

  // free mem
  for (int i = 0; i < nr_nodes_; i++)
    delete[] link_distances_[i];
  delete[] link_distances_;
  link_distances_ = nullptr;
}

void GreedyPathPlanner::initPlanner(Point start, Point goal, std::map <std::string, Part> parts)
{
  Part current_part;
  float dx, dy;
  int idx = 1;

  nr_nodes_ = parts.size() + 2; // + 2 includes start, goal

  // allocate memory for distances
  link_distances_ = new float* [nr_nodes_];
  for(int i = 0; i < nr_nodes_; i++)
    link_distances_[i] = new float [nr_nodes_];

  // get all nodes 
  current_part.dest_x_ = start.x_;
  current_part.dest_y_ = start.y_;
  nodes_.push_back(std::make_pair(0, current_part));
  for(auto it = parts.begin(); it != parts.end(); it++)
  {
    current_part = it->second;
    nodes_.push_back(std::make_pair(idx++, current_part));
  }
  current_part.dest_x_ = goal.x_;
  current_part.dest_y_ = goal.y_;
  nodes_.push_back(std::make_pair(nr_nodes_-1, current_part));

  // calc distances to all links
  for(int x = 0; x < nr_nodes_; x++)
    for(int y = 0; y < nr_nodes_; y++)
    {
      dx = nodes_[x].second.dest_x_ - nodes_[y].second.dest_x_;
      dy = nodes_[x].second.dest_y_ - nodes_[y].second.dest_y_;
      link_distances_[x][y] = sqrt(dx*dx + dy*dy);
    }
}

void GreedyPathPlanner::planPath(std::vector<Part>* path)
{
  float short_dist = __FLT_MAX__;
  int idx, del_idx;

  // get start and end position and delete them
  auto current_node = nodes_.front();
  Part goal_node = nodes_.back().second;
  nodes_.erase(nodes_.begin());
  nodes_.erase(nodes_.end());

  // add start node to path & update index
  path->push_back(current_node.second); 
  idx = current_node.first;

  while(!nodes_.empty())
  {
    // find nearest node to actual position
    for(unsigned int i = 0; i < nodes_.size(); i++)
    {
      auto node = nodes_[i];

      if(link_distances_[idx][node.first] < short_dist)
      {
        current_node = node;
        del_idx = i;
        short_dist = link_distances_[idx][node.first];
      }
    }

    // add node to path & update index
    path->push_back(current_node.second); 
    idx = current_node.first;

    // remove node from list
    nodes_.erase(nodes_.begin() + del_idx);

    // reset stuff
    short_dist = __FLT_MAX__; 
  }

  // finally, add goal node
  path->push_back(goal_node); 
}
